#!/bin/sh


set -eu

SVN_BRANCH=upstream-svn/B2_5_Release

HEAD=`git log --format=oneline $SVN_BRANCH -1|head -1|cut -d" " -f1`
REV=`git log --format=full $HEAD -1 | grep 'git-svn-id: ' | sed -e 's/.\+@//; s/ .\+//'`
DATE=`git show --pretty=format:%ai $HEAD | awk '{print $1; exit;}'|sed 's/-//g'`

echo upstream-svn head is $HEAD, r$REV from $DATE

if [ -n "${1:-}" ];
then
    echo Syntax: $0
    exit 1
fi

upstream_ver() {
    COMPONENT=$1
    local VER_FILE=src/jrd/build_no.h
    RES=`git show $SVN_BRANCH:$VER_FILE | awk "/^#define FB_$COMPONENT / { print \\$3 }" | sed 's,\",,g'`
    if [ -z "$RES" ]; then
        echo "FB_$COMPONENT not found in $SVN_BRANCH:$VER_FILE:"
        git show $SVN_BRANCH:$VER_FILE
        exit 1
    else
        echo $RES
    fi
}

VER=`upstream_ver MAJOR_VER`.`upstream_ver MINOR_VER`.`upstream_ver REV_NO`

echo "Version is $VER"

TAR="firebird3.0-$VER~svn+${REV}"

git archive --format=tar --prefix=$TAR/ $HEAD | xz > ../$TAR.tar.xz

sh debian/repack.sh --upstream-version $VER~svn+${REV} ../$TAR.tar.xz
