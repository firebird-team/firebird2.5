/* Prune Firebird database file from random garbage
   Copyright (c) 2015 Damyan Ivanov <dmn@debian.org>
   Permission is granted to use this work, with or without modifications,
   provided that this notice is retained. If we meet some day, and you think this
   stuff is worth it, you can buy me a beer in return.
 */

#include "gen/autoconfig.h"
#include "fb_types.h"
#include "src/jrd/common.h"
#include "src/jrd/ods.h"
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <time.h>
#include <ibase.h>

int main(int argc, char** argv) {
    FILE *f;
    Ods::header_page pag;
    tm time;
    int page_size;

    if( argc != 3 ) {
        error( 1, 0, "expecting exactly two arguments - database file path and 'yyyy-mm-dd' date" );
    }

    f = fopen(argv[1], "r+");

    if (f == NULL) {
        error( 2, errno, "fopen(%s)", argv[1] );
    }

    if ( fread( &pag, sizeof(pag), 1, f ) != 1 ) {
        error( 3, errno, "Read error" );
    }

    fprintf( stdout, "Page type is %d\n", pag.hdr_header.pag_type );
    if ( pag.hdr_header.pag_type != pag_header )
        error( 4, 0, "Unexpected page type" );

    fprintf( stdout, "ODS is %d.%d\n", pag.hdr_ods_version & ~ODS_FIREBIRD_FLAG, pag.hdr_ods_minor );

    if ( (pag.hdr_ods_version & ~ODS_FIREBIRD_FLAG) != 11 )
        error( 4, 0, "Unsupported ODS version" );

    page_size = pag.hdr_page_size;
    fprintf(stdout, "Page size: %d (0x%x)\n", page_size, page_size );

    isc_decode_timestamp( (ISC_TIMESTAMP *)pag.hdr_creation_date, &time);
    fprintf(stdout, "Creation: %d.%d.%d %d:%02d:%02d\n",
                           time.tm_mday, time.tm_mon + 1, time.tm_year + 1900,
                           time.tm_hour, time.tm_min, time.tm_sec);

    if ( strptime( argv[2], "%Y-%m-%d", &time ) == NULL )
        error( 5, 0, "Unable to parse timestamp" );
    time.tm_sec = 0;
    time.tm_min = 0;
    time.tm_hour = 0;
    time.tm_isdst = 0;

    isc_encode_timestamp( &time, (ISC_TIMESTAMP *)pag.hdr_creation_date );

    if ( fseek( f, 0, SEEK_SET ) != 0 )
        error( 6, errno, "fseek(0)" );

    if ( fwrite( &pag, sizeof(pag), 1, f ) != 1 )
        error( 7, errno, "fwrite" );

    // read all pages, pruning b-tree index pages and data pages
    {
        int page_no = 0;
        typedef union {
            unsigned char bytes[MAX(sizeof(Ods::pag), MAX(sizeof(Ods::data_page), sizeof(Ods::btree_page)))];
            Ods::pag header;
            Ods::data_page data;
            Ods::btree_page btree;
        } PAGE;
        PAGE *pbuf;
        bool *pmap;

        pbuf = (PAGE*) malloc( page_size );
        if (pbuf == NULL )
            error( 8, 0, "Unable to allocate page buffer" );

        pmap = (bool*) malloc( page_size * sizeof(bool) );
        if (pmap == NULL )
            error( 8, 0, "Unable to allocate page map" );

        while(true) {
            page_no++;
            if ( fseek( f, page_no * page_size, SEEK_SET ) != 0 )
                error( 8, errno, "fseek(page %d)", page_no );

            if ( fread( pbuf, page_size, 1, f ) != 1 ) {
                if ( feof(f) )
                    break;
                error( 8, errno, "Error reading page %d", page_no );
            }

            switch (pbuf->header.pag_type) {
                case pag_index:
                    fprintf( stdout, "Index page %d (0x%x) for relation %d\n", page_no, page_no, pbuf->btree.btr_relation );
                    if ( pbuf->btree.btr_length < page_size ) {
                        int wipe_size = page_size - pbuf->btree.btr_length;
                        fprintf( stdout, " 0x%x bytes used; wiping 0x%x bytes\n", pbuf->btree.btr_length, wipe_size );
                        memset( pbuf->bytes + pbuf->btree.btr_length, 0, wipe_size );
                    }
                    break;
                case pag_data: {
                    int min_data_offset = offsetof( Ods::data_page, dpg_rpt ) + pbuf->data.dpg_count * sizeof(pbuf->data.dpg_rpt[0]);
                    memset( pmap, 0, page_size * sizeof(bool) );
                    fprintf( stdout, "Data page %d (0x%x) for relation %d, with %d fragments, data starts at 0x%x\n", page_no, page_no, pbuf->data.dpg_relation, pbuf->data.dpg_count, min_data_offset );
                    for(int i = 0; i < min_data_offset; i++ ) pmap[i] = true;

                    for( int frag = 0; frag < pbuf->data.dpg_count; frag++ ) {
                        if (pbuf->data.dpg_rpt[frag].dpg_offset == 0)
                            continue;

                        for(int i = 0; i < pbuf->data.dpg_rpt[frag].dpg_length; i++ ) pmap[pbuf->data.dpg_rpt[frag].dpg_offset + i] = true;
                    }

                    for(int i = 0; i < page_size; i++) {
                        if (!pmap[i])
                            pbuf->bytes[i] = 0;
                    }
                    break;
                }
                default:
                    continue;
            }

            if ( fseek( f, page_no * page_size, SEEK_SET ) != 0 )
                error( 8, errno, "fseek(page %d)", page_no );
            if ( fwrite( pbuf, page_size, 1, f ) != 1 )
                error( 8, errno, "fwrite(page %d)", page_no );
        }
    }

    fclose(f);

    return 0;
}
