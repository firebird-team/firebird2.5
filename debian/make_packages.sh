#!/bin/sh

set -eu

get_ver()
{
	awk "/FB_$1/ { v=\$3; gsub(\"\\\"\", \"\", v); print v}" < src/jrd/build_no.h
}

FB_MAJOR=$( get_ver 'MAJOR_VER' )
FB_MINOR=$( get_ver 'MINOR_VER' )
FB_REV=$( get_ver 'REV_NO' )

FB_VERSION="${FB_MAJOR}.${FB_MINOR}.${FB_REV}"

FB_VER="${FB_MAJOR}.${FB_MINOR}"
FB2="firebird$FB_VER"
FB2_no_dots=`echo $FB2 | sed -e 's/\.//g'`
FB2DIR="firebird/$FB_VER"
ULFB2="usr/lib/$FB2DIR"
USFB2="usr/share/$FB2DIR"
VAR="var/lib/$FB2DIR"
EMBED_SO_VER="${FB_MAJOR}.${FB_MINOR}"
CLIENT_SO_VER=${FB_MAJOR}
UTIL_SO_VER=


copy ()
{
    type=$1
    dest=$2
    shift
    shift

    case "$type" in
        e*) mode="755" ;;
        f*) mode="644" ;;
        *) echo "Error: Wrong params for copy!"; exit 1;;
    esac

    install -m $mode "$@" "$dest"
}

# Helper function used both in -super and -classic
copy_utils()
{
    for s in gbak gdef gfix gpre qli gsec gstat isql nbackup ;
    do
        target=$s
        if [ $target = gstat ];
        then
            target=fbstat
        elif [ $target = isql ];
        then
            target=isql-fb
        fi

        copy e $D/usr/bin/$target $S/usr/bin/$s
    done
}

COMMON_DOC=/usr/share/doc/$FB2-common-doc

doc_symlink() {
    local doc_root
    doc_root=debian/$P/usr/share/doc
    [ -d $doc_root ] || mkdir -p $doc_root
    ln -s $FB2-common-doc $doc_root/$P
}

#-super
make_super () {
    P="$FB2-super"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/bin $D/usr/sbin $D/$ULFB2/UDF

    copy e $D/usr/sbin              \
        $S/usr/sbin/fb_lock_print   \
        $S/usr/sbin/fbserver        \

    copy e $D/usr/bin       \
        $S/usr/bin/fbsvcmgr \
        $S/usr/bin/fbtracemgr

    copy e $D/$ULFB2/UDF \
        $S/$ULFB2/UDF/fbudf.so $S/$ULFB2/UDF/ib_udf.so

    copy_utils

    dh_installman -p $P debian/fbserver.1

    doc_symlink

    ls -lR $D
}

#-classic
make_classic () {
    P="$FB2-classic"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-classic

    mkdir -p $D/usr/sbin    \
             $D/etc/xinetd.d

    copy e $D/usr/sbin \
        $S/usr/sbin/fb_inet_server

    dh_installman -p $P debian/fb_inet_server.1

    install -m 0644 debian/$FB2-classic.xinetd \
    		    $D/etc/xinetd.d/$FB2_no_dots

    doc_symlink

    ls -lR $D
}

#-superclassic
make_superclassic () {
    P="$FB2-superclassic"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-classic

    mkdir -p $D/usr/sbin

    copy e $D/usr/sbin \
        $S/usr/sbin/fb_smp_server

    dh_installman -p $P debian/fb_smp_server.1

    doc_symlink

    ls -lR $D
}

#-classic-common
make_classic_common () {
    P="$FB2-classic-common"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-classic

    mkdir -p $D/$ULFB2/UDF      \
             $D/usr/bin         \
             $D/usr/sbin

    copy e $D/usr/sbin \
	$S/usr/sbin/fb_lock_print

    copy e $D/usr/bin \
	$S/usr/bin/fbsvcmgr

    copy e $D/usr/bin \
        $S/usr/bin/fbtracemgr

    copy_utils

    copy e $D/$ULFB2/UDF $S/$ULFB2/UDF/fbudf.so $S/$ULFB2/UDF/ib_udf.so

    doc_symlink

    ls -lR $D
}


#libfbclient
make_libfbclient () {
    P="libfbclient$CLIENT_SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/lib/$DEB_HOST_MULTIARCH

    copy e $D/usr/lib/$DEB_HOST_MULTIARCH $S/usr/lib/$DEB_HOST_MULTIARCH/libfbclient.so.$FB_VERSION
    ln -s libfbclient.so.$FB_VERSION $D/usr/lib/$DEB_HOST_MULTIARCH/libfbclient.so.$CLIENT_SO_VER

    doc_symlink

    ls -lR $D
}

#libfbembed
make_libfbembed () {
    P="libfbembed$EMBED_SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-classic

    mkdir -p $D/usr/lib/$DEB_HOST_MULTIARCH

    copy e $D/usr/lib/$DEB_HOST_MULTIARCH $S/usr/lib/$DEB_HOST_MULTIARCH/libfbembed.so.$FB_VERSION
    ln -s libfbembed.so.$FB_VERSION $D/usr/lib/$DEB_HOST_MULTIARCH/libfbembed.so.$EMBED_SO_VER

    doc_symlink

    ls -lR $D
}

#libib-util
make_libib_util () {
    P="libib-util$UTIL_SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/lib/$DEB_HOST_MULTIARCH

    install -m 0755 $S/usr/lib/$DEB_HOST_MULTIARCH/libib_util.so $D/usr/lib/$DEB_HOST_MULTIARCH/

    doc_symlink

    ls -lR $D
}


#-server-common
make_server_common () {
    P="$FB2-server-common"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/etc/$FB2DIR \
        $D/etc/logrotate.d \
        $D/etc/default \
        $D/etc/$FB2DIR \
        $D/usr/sbin \
        $D/$ULFB2 \
        $D/$ULFB2/UDF \
        $D/$ULFB2/intl \
        $D/$ULFB2/plugins \
        $D/$VAR \
        $D/$VAR/system \
        $D/$VAR/tmp \
        $D/$VAR/data \
        $D/$VAR/backup \
        $D/$COMMON_DOC/examples \
        $D/var/log/firebird

    copy f $D/etc/$FB2DIR \
        $S/etc/$FB2DIR/aliases.conf \
        $S/etc/$FB2DIR/fbtrace.conf

    cp debian/server.default $D/etc/default/firebird${FB_VER}

    # fix aliases.conf: employee.fdb should point to a database
    # in /$VAR/data where all databases live
    sed -i -e "s,/$ULFB2/examples/empbuild,/$VAR/data," \
        $D/etc/$FB2DIR/aliases.conf

    touch $D/$VAR/backup/no_empty
    touch $D/$VAR/data/no_empty

    copy f $D/$ULFB2/UDF \
        $S/$ULFB2/UDF/*.sql

    copy e $D/usr/sbin              \
	$S/usr/sbin/fbguard         \

    copy f $D/$ULFB2/plugins    \
        $S/$ULFB2/plugins/libfbtrace.so

    copy f $D/etc/$FB2DIR/fbintl.conf $S/$ULFB2/intl/fbintl.conf

    install -m 0644 $S/$ULFB2/intl/fbintl $D/$ULFB2/intl/fbintl.so

    # databases
    cp $S/$VAR/system/security2.fdb \
        $D/$VAR/system/default-security2.fdb

    copy f $D/$VAR/system $S/$VAR/system/help.fdb

    # manpages
    for u in fbstat gbak gdef gsec isql-fb gfix gpre qli nbackup fbsvcmgr \
        fbtracemgr fbguard fb_lock_print ;
    do
        dh_installman -p $P debian/$u.1
    done

    copy f $D/$COMMON_DOC/examples debian/reindex-db

    doc_symlink

    ls -lR $D
}

#-common
make_common () {
    P="$FB2-common"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p \
        $D/etc/$FB2DIR \
        $D/usr/lib/$DEB_HOST_MULTIARCH/$FB2DIR \
        $D/usr/share/$P

    # config
    copy f $D/etc/$FB2DIR $S/etc/$FB2DIR/firebird.conf
    sed -i -e '/^#RemoteBindAddress/ a RemoteBindAddress = localhost' $D/etc/$FB2DIR/firebird.conf

    install -m 0644 -o root -g root \
        debian/functions.sh \
        $D/usr/share/$P/

    for m in $S/usr/lib/$DEB_HOST_MULTIARCH/$FB2DIR/*.msg;
    do
        copy f $D/usr/lib/$DEB_HOST_MULTIARCH/$FB2DIR $m
    done

    doc_symlink

    ls -lR $D
}

#-dev
make_dev () {
    P="firebird-dev"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/include \
        $D/usr/lib/$DEB_HOST_MULTIARCH \
        $D/usr/bin \
        $D/usr/share/man/man1

    copy f $D/usr/include $S/usr/include/*
    copy e $D/usr/bin $S/usr/sbin/fb_config
    help2man -o $D/usr/share/man/man1/fb_config.1 -s 1 -N \
        -n 'information about firebird configuration' \
        $D/usr/bin/fb_config
    sed -i -e 's,debian/firebird-dev/usr/bin/,,' $D/usr/share/man/man1/fb_config.1

    ln -s libfbclient.so.$FB_VERSION $D/usr/lib/$DEB_HOST_MULTIARCH/libfbclient.so
    ln -s libfbembed.so.$FB_VERSION $D/usr/lib/$DEB_HOST_MULTIARCH/libfbembed.so
    if [ -n "$UTIL_SO_VER" ]; then
        ln -s libib_utill.so.$UTIL_SO_VER $D/usr/lib/$DEB_HOST_MULTIARCH/libib_util.so
    fi

    doc_symlink

    ls -lR $D
}


#-examples
make_examples () {
    P="$FB2-examples"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/$COMMON_DOC
    cp -r $S/$COMMON_DOC/examples $D/$COMMON_DOC

    install -m 0644 \
        debian/$P.README.Debian \
        $D/$COMMON_DOC/examples/README.Debian

    doc_symlink

    ls -lR $D
}

#-doc
make_doc () {
    P="$FB2-doc"
    echo "Creating $P content"
    D=debian/$P/$COMMON_DOC/doc
    S=doc

    mkdir -p $D

    cp -r $S/* $D/
    rm -r $D/license

    doc_symlink

    ls -lR $D
}

#-common-doc
make_common_doc() {
    P="$FB2-common-doc"
    echo "Creating $P content"
    mkdir -p debian/$P/$COMMON_DOC
}

echo "*****************"
echo "* built classic *"
echo "*****************"

ls -lR debian/firebird-classic

echo "***************"
echo "* built super *"
echo "***************"

ls -lR debian/firebird-super

umask 022
make_super
make_classic
make_superclassic
make_classic_common
make_libfbclient
make_libfbembed
make_libib_util
make_common
make_server_common
make_dev
make_examples
make_doc
make_common_doc
P=$FB2-super-dbg doc_symlink
P=$FB2-classic-dbg doc_symlink
P=firebird${FB_VER}-dev doc_symlink
P=libfbclient$CLIENT_SO_VER-dbg doc_symlink
echo "Packages ready."
exit 0
