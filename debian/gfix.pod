=head1 NAME

gfix - Firebird database maintenance tool

=head1 SYNOPSIS

gfix I<option>... I<database>

=head1 DESCRIPTION

B<gfix> is a tool which performs a number of maintenance
activities on a database eg. database shutdown and making minor
data repairs.

=head1 OPTIONS

=head2 Common options

=over

=item B<-user> I<username>

=item B<-pa[ssword]> I<password>

User name and password for connecting to Firebird.

=item B<-z>

Print software version number.

=back

=head2 Database parameters

=over

=item B<-b[uffers]> I<n>

Set page buffers to I<n> pages.

=item B<-h[ousekeeping]> I<n>

Set sweep interval to I<n> transactions.

=item B<-s[ql_dialect]> I<n>

Set database dialect to I<n>.

=item B<-m[ode]> C<read_write|read_only>

Switch database to read/write or read-only mode.

=item B<-use>

Use full or reserve space for versions.

=item B<-w[rite]> C<sync>|C<async>

Switchd database to synchronous or asynchronous write mode.

=back

=head2 Database sweep (garbage collection)

=over

=item B<-s[weep]>

Perform immediate garbage collection.

=back

=head2 Shadow file maintenance

=over

=item B<-activate>

Acivate shadow file for database usage

=item B<-kill>

Kill all unavailable shadow files.

=back

=head2 Validation

=over

=item B<-v[alidate]>

Turn on validation mode

=item B<-f[ull]>

Validate record fragments.

=item B<-i[gnore]>
Ignore checksum errors.

=item B<-n[o_update]>

Perform read-only validation, without writing any corrections.

=item B<-l[ist]>

Show limbo transactions.

=item B<-c[ommit]> I<ID>|C<all>

Commit transaction I<ID> (or all).

=item B<-r[ollback]> I<ID>|C<all>

Rollback transaction numver I<ID> (or all).

=item B<-p[rompt>

Prompt for commit/rollback for each transaction that is in limbo.

=item B<-t[wo-phase]> I<ID>|C<all>

Perform automated two-phase recovery for transaction I<ID> (or
all).

=item B<-m[end]>

Prepare corrupt database for backup.

=back

=head1 Database shutdown

=over

=item B<-sh[ut]>

Shutdown database.

=item B<-at[tach]>

Prevent new database attachments.

=item B<-tr[an]>

Prevent starting of new transactions.

=item B<-f[orce]>

Force database shutdown.

=item B<-o[nline]>

Bring the database online (the opposite of shutdown).

=item B<-ca[che]>

Shutdown cache manager.

=back

=head1 AUTHOR AND LICENSE

Copyright (c) 2015 Damyan Ivanov E<lt>dmn@debian.orgE<gt>.

Permission is granted to use this document, with or without
modifications, provided that this notice is retained. If we meet
some day, and you think this stuff is worth it, you can buy me a
beer in return.
